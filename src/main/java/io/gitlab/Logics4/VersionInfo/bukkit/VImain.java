package io.gitlab.logics4.versioninfo.bukkit;

import java.util.UUID;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import io.gitlab.logics4.versioninfo.ConfigVersion;
import io.gitlab.logics4.versioninfo.bukkit.commands.VersioninfoCommand;
import org.bstats.bukkit.MetricsLite;
import us.myles.ViaVersion.api.Via;
import us.myles.ViaVersion.api.ViaAPI;

public class VImain extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {

        //Loads bStats Lite metrics.
        @SuppressWarnings("unused")
        MetricsLite metrics = new MetricsLite(this);

        //Creates config.
        saveDefaultConfig();

        //Loads config.
        this.getConfig();

        /*
	 * Variable referencing the console.
         * Useful to send messages or to do other stuff to the console.
         */
        ConsoleCommandSender console = getServer().getConsoleSender();

        // If "config-version" value from configuration file is different than the one returned by the getConfigVersion() method from 'ConfigVersion.java' class.
        if (getConfig().getInt("config-version") != ConfigVersion.getConfigVersion()) {
            console.sendMessage("§5§m-----------------------------------------------------");
            console.sendMessage("§6[§eVersionInfo§6]");
            console.sendMessage("§e'config-version' from config.yml is §6" + getConfig().getInt("config-version") + "§e, but it should be §6" + ConfigVersion.getConfigVersion() + "§e.");
            console.sendMessage("§eThis is normal if you updated or downloaded the plugin, or if you changed it by yourself..");
            console.sendMessage("§eIf you changed it by yourself, you can change it back to the original value,");
            console.sendMessage("§ehowever if you updated (or downgraded) the plugin we recommend you to reset");
            console.sendMessage("§ethe configuration to prevent features from not working properly (and also to");
            console.sendMessage("§esimply prevent confusion).");
            console.sendMessage(" ");
            console.sendMessage("§fIf you think this is an issue, please report it here:");
            console.sendMessage("§9https://gitlab.com/Logics4/VersionInfo/issues");
            console.sendMessage("§5§m-----------------------------------------------------");
        }

        //Register EventHandler from method "onJoin", from this class.
        getServer().getPluginManager().registerEvents(this, this);

        this.getCommand("versioninfo").setExecutor(new VersioninfoCommand());
        /*
	 * This registers the plugin's command.
	 * All the code is on "io/gitlab/Logics4/VersionInfo/bukkit/commands/VersioninfoCommand.java".
         */
    }

    @Override
    public void onDisable() {

    }

    // Event required to send message to player on join.
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        FileConfiguration config = getConfig(); //Shorten the way to use config-related stuff.
        ViaAPI<?> api = Via.getAPI(); //Shorten the way to use the API.

        UUID PlayerUUID2 = event.getPlayer().getUniqueId(); //Get player UUID, required to see their version.
        Player PlayerName2 = event.getPlayer(); //Get player name, required to send message.

        if (config.getBoolean("joinmessages-enabled")) { //If messages in general are enabled on the config.

            if (config.getBoolean("1_8_X-enabled")) { //If messages for 1.8.X are enabled on the config.
                if (api.getPlayerVersion(PlayerUUID2) == 47) {
                    PlayerName2.sendMessage(config.getString("1_8_X-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_9-enabled")) { //If messages for 1.9 are enabled on the config.
                if (api.getPlayerVersion(PlayerUUID2) == 107) {
                    PlayerName2.sendMessage(config.getString("1_9-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_9_1-enabled")) { //Same but for 1.9.1.
                if (api.getPlayerVersion(PlayerUUID2) == 108) {
                    PlayerName2.sendMessage(config.getString("1_9_1-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_9_2-enabled")) { //Same but for 1.9.2.
                if (api.getPlayerVersion(PlayerUUID2) == 109) {
                    PlayerName2.sendMessage(config.getString("1_9_2-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_9_3or4-enabled")) { // 1.9.3/1.9.4
                if (api.getPlayerVersion(PlayerUUID2) == 110) {
                    PlayerName2.sendMessage(config.getString("1_9_3or4-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_10_X-enabled")) { //1.10.X
                if (api.getPlayerVersion(PlayerUUID2) == 210) {
                    PlayerName2.sendMessage(config.getString("1_10_X-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_11-enabled")) { //1.11
                if (api.getPlayerVersion(PlayerUUID2) == 315) {
                    PlayerName2.sendMessage(config.getString("1_11-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_11_1or2-enabled")) { // 1.11.1/1.11.2
                if (api.getPlayerVersion(PlayerUUID2) == 316) {
                    PlayerName2.sendMessage(config.getString("1_11_1or2-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_12-enabled")) { //1.12
                if (api.getPlayerVersion(PlayerUUID2) == 335) {
                    PlayerName2.sendMessage(config.getString("1_12-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_12_1-enabled")) { //1.12.1
                if (api.getPlayerVersion(PlayerUUID2) == 338) {
                    PlayerName2.sendMessage(config.getString("1_12_1-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_12_2-enabled")) { //1.12.2
                if (api.getPlayerVersion(PlayerUUID2) == 340) {
                    PlayerName2.sendMessage(config.getString("1_12_2-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_13-enabled")) { //1.13
                if (api.getPlayerVersion(PlayerUUID2) == 393) {
                    PlayerName2.sendMessage(config.getString("1_13-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_13_1-enabled")) { //1.13.1
                if (api.getPlayerVersion(PlayerUUID2) == 401) {
                    PlayerName2.sendMessage(config.getString("1_13_1-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_13_2-enabled")) { //1.13.2
                if (api.getPlayerVersion(PlayerUUID2) == 404) {
                    PlayerName2.sendMessage(config.getString("1_13_2-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_14-enabled")) { // 1.14
                if (api.getPlayerVersion(PlayerUUID2) == 477) {
                    PlayerName2.sendMessage(config.getString("1_14-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_14_1-enabled")) { // 1.14.1
                if (api.getPlayerVersion(PlayerUUID2) == 480) {
                    PlayerName2.sendMessage(config.getString("1_14_1-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_14_2-enabled")) { // 1.14.2
                if (api.getPlayerVersion(PlayerUUID2) == 485) {
                    PlayerName2.sendMessage(config.getString("1_14_2-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("lowerthan-enabled")) { //If messages for versions lower than 1.8.X are enabled.
                if (api.getPlayerVersion(PlayerUUID2) < 47) {
                    PlayerName2.sendMessage(config.getString("lowerthan-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("higherthan-enabled")) { //If messages for versions higher than 1.14.2 are enabled.
                if (api.getPlayerVersion(PlayerUUID2) > 485) {
                    PlayerName2.sendMessage(config.getString("higherthan-message").replace("&", "§"));
                    return;
                }
            }
        }
    }
}
