package io.gitlab.logics4.versioninfo.bukkit.commands;

import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import io.gitlab.logics4.versioninfo.ConfigVersion;
import us.myles.ViaVersion.api.Via;
import us.myles.ViaVersion.api.ViaAPI;

public class VersioninfoCommand implements CommandExecutor {

    //Method called if player doesn't have permission.
    public static void noPermission(CommandSender sender) {
        sender.sendMessage("§6[§eVersionInfo§6] §cYou do not have permission to execute this command!");
    }

    //Method to avoid code duplication ("/versioninfo" and "/versioninfo help").
    public static void help(CommandSender sender) {
        sender.sendMessage("§6[§eVersionInfo§6]");
        sender.sendMessage("§f/versioninfo help (or /versioninfo) - §7Displays this message!");
        sender.sendMessage("§f/versioninfo about - §7Shows some information about this plugin.");
        sender.sendMessage("§f/versioninfo reload - §7Reloads and applys the configuration file.");
        sender.sendMessage("§f/versioninfo [playername] - §7Tells you the version of a specific player.");
        sender.sendMessage("§fYou can also use \"/vi\" as a shortened version of the command.");
    }

    ViaAPI<?> api = Via.getAPI(); //Shorten the way to use the API.

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) { //What to do if there are no arguments ("/versioninfo").
            if (sender.hasPermission("versioninfo.use")) {
                help(sender);
                return true;
            } else {
                noPermission(sender);
                return true;
            }
        }
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("help")) { // "/versioninfo help"
                if (sender.hasPermission("versioninfo.use")) {
                    help(sender);
                    return true;
                } else {
                    noPermission(sender);
                    return true;
                }
            }
            if (args[0].equalsIgnoreCase("about")) { // "/versioninfo about"
                if (sender.hasPermission("versioninfo.use")) {
                    sender.sendMessage("§6[§eVersionInfo§6]");
                    sender.sendMessage("§fVersion: §71.2.14");
                    sender.sendMessage("§fAuthor: §7Logics4");
                    sender.sendMessage("§fDescription: §7Check the version of a specific player!");
                    return true;
                } else {
                    noPermission(sender);
                    return true;
                }
            }
            if (args[0].equalsIgnoreCase("reload")) { // "versioninfo reload"
                if (sender.hasPermission("versioninfo.reload")) {
                    Bukkit.getPluginManager().getPlugin("VersionInfo").saveDefaultConfig(); //If file doesn't exist on reload.

                    //Reloads config.
                    Bukkit.getPluginManager().getPlugin("VersionInfo").reloadConfig();

                    /*
						 * Variable referencing the console.
						 * Useful to send messages or to do other stuff to the console.
                     */
                    ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();

                    // If "config-version" value from configuration file is different than the one returned by the getConfigVersion() method from 'ConfigVersion.java' class.
                    if (Bukkit.getPluginManager().getPlugin("VersionInfo").getConfig().getInt("config-version") != ConfigVersion.getConfigVersion()) {
                        console.sendMessage("§5§m-----------------------------------------------------");
                        console.sendMessage("§6[§eVersionInfo§6]");
                        console.sendMessage("§e'config-version' from config.yml is §6" + Bukkit.getPluginManager().getPlugin("VersionInfo").getConfig().getInt("config-version") + "§e, but it should be §6" + ConfigVersion.getConfigVersion() + "§e.");
                        console.sendMessage("§eThis is normal to happen if you updated (or downgraded) the");
                        console.sendMessage("§eplugin or if you for some reason changed it by yourself.");
                        console.sendMessage("§eIF you changed it by yourself, we recommend you to change it to the");
                        console.sendMessage("§eoriginal value. IF you didn't (and/or you don't know why this is");
                        console.sendMessage("§ehappening), you should make a backup of your config.yml so you");
                        console.sendMessage("§edon't lose your custom settings, delete it and then restart the");
                        console.sendMessage("§eserver or reload the plugin so it re-generates the config.");
                        console.sendMessage(" ");
                        console.sendMessage("§eIf you think this is an issue, report it here:");
                        console.sendMessage("§9https://gitlab.com/Logics4/VersionInfo/issues");
                        console.sendMessage("§5§m-----------------------------------------------------");

                        if (sender instanceof Player) { // If the one sending the command is a player (and not the console).
                            sender.sendMessage("§6[§eVersionInfo§6] §c'config-version' from config.yml is §4" + Bukkit.getPluginManager().getPlugin("VersionInfo").getConfig().getInt("config-version") + "§c, but it should be §4" + ConfigVersion.getConfigVersion() + "§c. Contact a server administrator about this and tell them to read the console.");
                        }
                    }

                    sender.sendMessage("§6[§eVersionInfo§6] §fConfig reloaded and applied successfully.");
                    return true;
                } else {
                    noPermission(sender);
                    return true;
                }
            } else { //Assumes that the first argument is a player name and starts checking for its protocol version.
                if (sender.hasPermission("versioninfo.use")) {
                    Player PlayerName = Bukkit.getPlayer(args[0]);
                    if (PlayerName == null) { //If player is offline.
                        sender.sendMessage("§6[§eVersionInfo§6] §cThis player is not online! §fTry other.");
                        return true;
                    }

                    UUID PlayerUUID = Bukkit.getPlayer(args[0]).getUniqueId();
                    if (api.getPlayerVersion(PlayerUUID) == 47) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.8.X§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 107) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.9§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 108) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.9.1§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 109) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.9.2§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 110) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.9.3§f/§b1.9.4§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 210) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.10.X§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 315) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.11§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 316) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.11.1§f/§b1.11.2§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 335) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.12§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 338) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.12.1§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 340) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.12.2§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 393) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.13§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 401) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.13.1§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 404) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.13.2§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 477) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.14§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 480) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.14.1§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 485) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.14.2§f.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) < 47) {
                        sender.sendMessage("§6[§eVersionInfo§6] §cCouldn't specify player's version!");
                        sender.sendMessage("We know that it's lower than 1.8.X.");
                        return true;
                    }
                    if (api.getPlayerVersion(PlayerUUID) > 485) {
                        sender.sendMessage("§6[§eVersionInfo§6] §cCouldn't specify player's version!");
                        sender.sendMessage("We know that it's higher than §b1.14.2§f.");
                        return true;
                    }
                } else {
                    noPermission(sender);
                    return true;
                }
            }
        }
        if (args.length >= 2) { //What to do if player inserts 2 or more arguments on the command
            sender.sendMessage("§6[§eVersionInfo§6] §cIncorrect command usage! §fDo \"/versioninfo help\" to see how it works.");
        }
        return true;
    }
}
