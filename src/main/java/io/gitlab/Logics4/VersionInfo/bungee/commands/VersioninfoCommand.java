package io.gitlab.logics4.versioninfo.bungee.commands;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import com.google.common.io.ByteStreams;

import io.gitlab.logics4.versioninfo.ConfigVersion;
import io.gitlab.logics4.versioninfo.bungee.VImain;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import us.myles.ViaVersion.api.Via;
import us.myles.ViaVersion.api.ViaAPI;

public class VersioninfoCommand extends Command {

    VImain plugin;

    public VersioninfoCommand(VImain instance) {
        super("versioninfo", null, new String[]{"vi"});
        plugin = instance;
    }

    //Method called if player doesn't have permission.
    @SuppressWarnings("deprecation")
    public static void noPermission(CommandSender sender) {
        sender.sendMessage("§6[§eVersionInfo§6] §cYou do not have permission to execute this command!");
    }

    //method to avoid code duplication ("/versioninfo" and "/versioninfo help")
    @SuppressWarnings("deprecation")
    public static void help(CommandSender sender) {
        sender.sendMessage("§6[§eVersionInfo§6]");
        sender.sendMessage("§f/versioninfo help (or /versioninfo) - §7Displays this message!");
        sender.sendMessage("§f/versioninfo about - §7Shows some information about this plugin.");
        sender.sendMessage("§f/versioninfo reload - §7Reloads and applys the configuration file.");
        sender.sendMessage("§f/versioninfo [playername] - §7Tells you the version of a specific player.");
        sender.sendMessage("§fYou can also use \"/vi\" as a shortened version of the command.");
    }

    ViaAPI<?> api = Via.getAPI(); //Shorten the way to get to the API

    @SuppressWarnings("deprecation")
    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length == 0) { //What to do if there are no arguments ("/versioninfo").
            if (sender.hasPermission("versioninfo.use")) {
                help(sender);
                return;
            } else {
                noPermission(sender);
                return;
            }
        }
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("help")) { // "/versioninfo help"
                if (sender.hasPermission("versioninfo.use")) {
                    help(sender);
                    return;
                } else {
                    noPermission(sender);
                    return;
                }

            }
            if (args[0].equalsIgnoreCase("about")) { // "/versioninfo about"
                if (sender.hasPermission("versioninfo.use")) {
                    sender.sendMessage("§6[§eVersionInfo§6]");
                    sender.sendMessage("§fVersion: §71.2.14");
                    sender.sendMessage("§fAuthor: §7Logics4");
                    sender.sendMessage("§fDescription: §7Check the version of a specific player!");
                    return;
                } else {
                    noPermission(sender);
                    return;
                }
            }
            if (args[0].equalsIgnoreCase("reload")) { // "/versioninfo reload"
                if (sender.hasPermission("versioninfo.reload")) {

                    File getDataFolder = ProxyServer.getInstance().getPluginManager().getPlugin("VersionInfo").getDataFolder();

                    //If config doesn't exist on reload.
                    if (!getDataFolder.exists()) {
                        getDataFolder.mkdir();
                    }
                    File configFile3 = new File(getDataFolder, "config.yml");
                    if (!configFile3.exists()) {
                        try {
                            configFile3.createNewFile();
                            try (InputStream is3 = ProxyServer.getInstance().getPluginManager().getPlugin("VersionInfo").getResourceAsStream("config.yml");
                                    OutputStream os3 = new FileOutputStream(configFile3)) {
                                ByteStreams.copy(is3, os3);
                            }
                        } catch (IOException e6) {
                            throw new RuntimeException("Unable to create configuration file.", e6);
                        }
                    }

                    // Get (load) config.
                    Configuration config = null;
                    try {
                        config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile3);
                    } catch (IOException e) {
                        throw new RuntimeException("Could not load or reload configuration file." + System.getProperty("line.separator") + e);
                    }

                    /*
                     * Variable referencing the console.
                     * Useful to send messages or to do other stuff to the console.
                     */
                    CommandSender console = plugin.getProxy().getConsole();

                    // If "config-version" value from configuration file is different than the one returned by the getConfigVersion() method from 'ConfigVersion.java' class.
                    if (config.getInt("config-version") != ConfigVersion.getConfigVersion()) {
                        console.sendMessage(new TextComponent("§5§m-----------------------------------------------------"));
                        console.sendMessage(new TextComponent("§6[§eVersionInfo§6]"));
                        console.sendMessage(new TextComponent("§e'config-version' from config.yml is §6" + config.getInt("config-version") + "§e, but it should be §6" + ConfigVersion.getConfigVersion() + "§e."));
                        console.sendMessage(new TextComponent("§eThis is normal to happen if you updated the plugin or if you"));
                        console.sendMessage(new TextComponent("§efor some reason changed it by yourself."));
                        console.sendMessage(new TextComponent("§eIF you changed it by yourself, we recommend you to change it to the"));
                        console.sendMessage(new TextComponent("§eoriginal value. IF you didn't (and/or you don't know why this is"));
                        console.sendMessage(new TextComponent("§ehappening), you should make a backup of your config.yml so you"));
                        console.sendMessage(new TextComponent("§edon't lose your custom settings, delete it and then restart the"));
                        console.sendMessage(new TextComponent("§eserver or reload the plugin, so it re-generates the config."));
                        console.sendMessage(new TextComponent(" "));
                        console.sendMessage(new TextComponent("§eIf you think this is an issue, report it here:"));
                        console.sendMessage(new TextComponent("§9https://gitlab.com/Logics4/VersionInfo/issues"));
                        console.sendMessage(new TextComponent("§5§m-----------------------------------------------------"));

                        if (sender instanceof ProxiedPlayer) { // If the one sending the command is a player (and not the console).
                            sender.sendMessage(new TextComponent("§6[§eVersionInfo§6] §c'config-version' from config.yml is §4" + config.getInt("config-version") + "§c, but it should be §4" + ConfigVersion.getConfigVersion() + "§c. Contact a server administrator about this and tell them to read the console."));
                        }
                    }

                    sender.sendMessage("§6[§eVersionInfo§6] §fConfig reloaded and applied successfully.");
                    return;
                } else {
                    noPermission(sender);
                    return;
                }
            } else { //Assumes that the first argument is a player name and starts checking for its protocol version.

                if (sender.hasPermission("versioninfo.use")) {
                    ProxiedPlayer PlayerName = ProxyServer.getInstance().getPlayer(args[0]);
                    if (PlayerName == null) { //If player is offline...
                        sender.sendMessage("§6[§eVersionInfo§6] §cThis player is not online! §fTry other.");
                        return;
                    }

                    UUID PlayerUUID = ProxyServer.getInstance().getPlayer(args[0]).getUniqueId();
                    if (api.getPlayerVersion(PlayerUUID) == 47) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.8.X§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 107) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.9§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 108) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.9.1§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 109) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.9.2§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 110) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.9.3§f/§b1.9.4§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 210) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.10.X§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 315) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.11§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 316) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.11.1§f/§b1.11.2§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 335) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.12§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 338) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.12.1§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 340) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.12.2§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 393) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.13§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 401) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.13.1§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 404) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.13.2§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 477) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.14§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 480) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.14.1§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) == 485) {
                        sender.sendMessage("§6[§eVersionInfo§6] §fThis player is running Minecraft §b1.14.2§f.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) < 47) {
                        sender.sendMessage("§6[§eVersionInfo§6] §cCouldn't specify player's version!");
                        sender.sendMessage("We know that it's lower than 1.8.X.");
                        return;
                    }
                    if (api.getPlayerVersion(PlayerUUID) > 485) {
                        sender.sendMessage("§6[§eVersionInfo§6] §cCouldn't specify player's version!");
                        sender.sendMessage("We know that it's higher than §b1.14.2§f.");
                        return;
                    }
                } else {
                    noPermission(sender);
                    return;
                }
            }
        }
        if (args.length >= 2) { //What to do if player inserts 2 or more arguments on the command.
            sender.sendMessage("§6[§eVersionInfo§6] §cIncorrect command usage! §fDo \"/versioninfo help\" to see how it works.");
        }
    }
}
