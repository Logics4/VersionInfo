package io.gitlab.logics4.versioninfo.bungee;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import com.google.common.io.ByteStreams;

import io.gitlab.logics4.versioninfo.ConfigVersion;
import io.gitlab.logics4.versioninfo.bungee.commands.VersioninfoCommand;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.md_5.bungee.event.EventHandler;
import us.myles.ViaVersion.api.Via;
import us.myles.ViaVersion.api.ViaAPI;
import org.bstats.bungeecord.MetricsLite;

public class VImain extends Plugin implements Listener {

    @Override
    public void onEnable() {

        //Loads bStats Lite metrics.
        @SuppressWarnings("unused")
        MetricsLite metrics = new MetricsLite(this);

        //Creates config.
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                try (InputStream is = getResourceAsStream("config.yml");
                        OutputStream os = new FileOutputStream(configFile)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to create configuration file.", e);
            }
        }

        // Get (load) config.
        Configuration config = null;
        try {
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
        } catch (IOException e) {
            throw new RuntimeException("Could not load or reload configuration file." + System.getProperty("line.separator") + e);
        }

        /*
	 * Variable referencing the console.
	 * Useful to send messages or to do other stuff to the console.
         */
        CommandSender console = getProxy().getConsole();

        // If "config-version" value from configuration file is different than the one returned by the getConfigVersion() method from 'ConfigVersion.java' class.
        if (config.getInt("config-version") != ConfigVersion.getConfigVersion()) {
            console.sendMessage(new TextComponent("§5§m-----------------------------------------------------"));
            console.sendMessage(new TextComponent("§6[§eVersionInfo§6]"));
            console.sendMessage(new TextComponent("§e'config-version' from config.yml is §6" + config.getInt("config-version") + "§e, but it should be §6" + ConfigVersion.getConfigVersion() + "§e."));
            console.sendMessage(new TextComponent("§eThis is normal if you updated or downloaded the plugin, or if you changed it by yourself.."));
            console.sendMessage(new TextComponent("§eIf you changed it by yourself, you can change it back to the original value,"));
            console.sendMessage(new TextComponent("§ehowever if you updated (or downgraded) the plugin we recommend you to reset"));
            console.sendMessage(new TextComponent("§ethe configuration to prevent features from not working properly (and also to"));
            console.sendMessage(new TextComponent("§esimply prevent confusion)."));
            console.sendMessage(new TextComponent(" "));
            console.sendMessage(new TextComponent("§fIf you think this is an issue, please report it here:"));
            console.sendMessage(new TextComponent("§9https://gitlab.com/Logics4/VersionInfo/issues"));
            console.sendMessage(new TextComponent("§5§m-----------------------------------------------------"));
        }

        //Register EventHandler from method "onJoin", from this class.
        getProxy().getPluginManager().registerListener(this, (Listener) this);

        /*
	 * Registers the command.
	 * Same as bukkit, almost all the code will be on VersioninfoCommand.java,
	 * on "io/gitlab/Logics4/VersionInfo/bungee/commands/".
         */
        getProxy().getPluginManager().registerCommand(this, new VersioninfoCommand(this));
    }

    @Override
    public void onDisable() {

    }

    // Event required to send message to player on join.
    @SuppressWarnings("deprecation")
    @EventHandler
    public void onJoin(PostLoginEvent event) throws IOException {

        Configuration config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml")); //Shorten the way to use config-related stuff.
        ViaAPI<?> api = Via.getAPI(); //Shorten the way to use the API.

        ProxiedPlayer PlayerName2 = event.getPlayer();
        UUID PlayerUUID2 = event.getPlayer().getUniqueId();

        if (config.getBoolean("joinmessages-enabled")) { //If messages in general are enabled on the config.

            if (config.getBoolean("1_8_X-enabled")) { //If messages for 1.8.X are enabled on the config.
                if (api.getPlayerVersion(PlayerUUID2) == 47) {
                    PlayerName2.sendMessage(config.getString("1_8_X-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_9-enabled")) { //If messages for 1.9 are enabled on the config.
                if (api.getPlayerVersion(PlayerUUID2) == 107) {
                    PlayerName2.sendMessage(config.getString("1_9-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_9_1-enabled")) { //Same but for 1.9.1.
                if (api.getPlayerVersion(PlayerUUID2) == 108) {
                    PlayerName2.sendMessage(config.getString("1_9_1-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_9_2-enabled")) { //Same but for 1.9.2.
                if (api.getPlayerVersion(PlayerUUID2) == 109) {
                    PlayerName2.sendMessage(config.getString("1_9_2-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_9_3or4-enabled")) { // 1.9.3/1.9.4
                if (api.getPlayerVersion(PlayerUUID2) == 110) {
                    PlayerName2.sendMessage(config.getString("1_9_3or4-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_10_X-enabled")) { //1.10.X
                if (api.getPlayerVersion(PlayerUUID2) == 210) {
                    PlayerName2.sendMessage(config.getString("1_10_X-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_11-enabled")) { //1.11
                if (api.getPlayerVersion(PlayerUUID2) == 315) {
                    PlayerName2.sendMessage(config.getString("1_11-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_11_1or2-enabled")) { // 1.11.1/1.11.2
                if (api.getPlayerVersion(PlayerUUID2) == 316) {
                    PlayerName2.sendMessage(config.getString("1_11_1or2-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_12-enabled")) { //1.12
                if (api.getPlayerVersion(PlayerUUID2) == 335) {
                    PlayerName2.sendMessage(config.getString("1_12-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_12_1-enabled")) { //1.12.1
                if (api.getPlayerVersion(PlayerUUID2) == 338) {
                    PlayerName2.sendMessage(config.getString("1_12_1-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_12_2-enabled")) { //1.12.2
                if (api.getPlayerVersion(PlayerUUID2) == 340) {
                    PlayerName2.sendMessage(config.getString("1_12_2-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_13-enabled")) { //1.13
                if (api.getPlayerVersion(PlayerUUID2) == 393) {
                    PlayerName2.sendMessage(config.getString("1_13-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_13_1-enabled")) { //1.13.1
                if (api.getPlayerVersion(PlayerUUID2) == 401) {
                    PlayerName2.sendMessage(config.getString("1_13_1-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_13_2-enabled")) { //1.13.2
                if (api.getPlayerVersion(PlayerUUID2) == 404) {
                    PlayerName2.sendMessage(config.getString("1_13_2-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_14-enabled")) { // 1.14
                if (api.getPlayerVersion(PlayerUUID2) == 477) {
                    PlayerName2.sendMessage(config.getString("1_14-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_14_1-enabled")) { // 1.14.1
                if (api.getPlayerVersion(PlayerUUID2) == 480) {
                    PlayerName2.sendMessage(config.getString("1_14_1-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("1_14_2-enabled")) { // 1.14.2
                if (api.getPlayerVersion(PlayerUUID2) == 485) {
                    PlayerName2.sendMessage(config.getString("1_14_2-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("lowerthan-enabled")) { //If messages for versions lower than 1.8.X are enabled.
                if (api.getPlayerVersion(PlayerUUID2) < 47) {
                    PlayerName2.sendMessage(config.getString("lowerthan-message").replace("&", "§"));
                    return;
                }
            }

            if (config.getBoolean("higherthan-enabled")) { // If messages for versions higher than 1.14.2 are enabled.
                if (api.getPlayerVersion(PlayerUUID2) > 485) {
                    PlayerName2.sendMessage(config.getString("higherthan-message").replace("&", "§"));
                    return;
                }
            }
        }
    }
}
