# VersionInfo (Bukkit/Bungee)
What does this plugin allow you to do? <br />
<br />
1 - ViaVersion has /viaversion list, which gives you the list of all the online players organized by their versions. This can be a hassle if your server has for example 500 players and you want to know the version of just one of them. **This plugin allows you to see the version of a specific player,** instead of just giving you the full player list.<br />

![VersionInfo-Demo-Image](https://i.imgur.com/6ilH1Xd.png)
<br />
<br />
2 - This plugin allows you to send a message to the player when they join, according to their version (for example, if the player joins using 1.8, it will say they joined with 1.8). <br />
This is globally disabled by default on config.yml, but you can enable, customize the messages and enable or disable specific version messages. <br />

![VersionInfo-Demo-Image2](https://i.imgur.com/vMj5T8i.png)
<br />
<br />
**Supported platforms at the moment:**
* Craftbukkit, Spigot, Paper, whatever software based on those;
* BungeeCord, Waterfall, whatever software based on those too. <br />

(In theory this plugin should work on server software that implements Bukkit API and that are supported by ViaVersion, but this might be wrong.) <br />
This plugin should support since version 1.8.8 until 1.13.2. Also, if ViaVersion is installed on BungeeCord, this plugin has to be on BungeeCord. If it's on Spigot, this plugin goes on Spigot. <br />
<br />
![VersionInfo-Help-Command](https://i.imgur.com/VHxLi4t.png)
<br />
**Permissions:**
* versioninfo.use - Use all the commands except "/versioninfo reload"; <br />
* versioninfo.reload - "/versioninfo reload". <br />

**Configuration:** https://pastebin.com/LRsSjXHD <br />

**Requirements:**
* Java 8;
* [ViaVersion](https://www.spigotmc.org/resources/viaversion.19254/) (latest release version recommended, although this might also work with DEV versions);
* A supported platform (duh?). <br />

<br />
You can also use this plugin if you have [ViaBackwards](https://www.spigotmc.org/resources/viabackwards.27448/) and/or [ViaRewind](https://www.spigotmc.org/resources/viarewind.52109/) installed.<br />

This plugin uses bStats metrics to collect anonymous data about your server. At the moment, we only collect the default data bStats usually collects:

    - Your server's randomly generated UUID;
    - The amount of players on your server;
    - The online mode of your server;
    - The bukkit version of your server;
    - The java version of your system (e.g. Java 8);
    - The name of your OS (e.g. Windows);
    - The version of your OS;
    - The architecture of your OS (e.g. amd64);
    - The system cores of your OS (e.g. 8);
    - bStats-supported plugins;
    - Plugin version of bStats-supported plugins.

If you're still that paranoid about privacy you can always disable bStats in plugins/bStats/config.yml, however you will be also disabling metrics for every other plugin that uses bStats in your server.<br />

**Ideas for the future:**
1. ~~Maybe add an option to send a customizable message to the player when they join using a specific version.~~ **(Available in version 1.2).**
2. Maybe support ProtocolSupport. <br />

<br />
**Download page:** https://www.spigotmc.org/resources/47999/ (SpigotMC Resources) <br />
<br />
If you find any bugs and want them to be solved, report them [here](https://gitlab.com/Logics4/VersionInfo/issues).
